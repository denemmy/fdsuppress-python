#!usr/bin/env python2

import pyfacesdk as facesdk
import pyvideoio as videoio
import time
import os
import cv2
import numpy as np
from argparse import ArgumentParser
import scipy
import collections

def to_bbox_list(faces):
	bbox_list = list()
	confs = list()
	for face in faces:
		bbox_list.append((face.x, face.y, face.w, face.h))
		confs.append(face.score)
	return bbox_list, confs

def list_diff(list1, list2):
	s = set(list2)
	list3 = list()
	mask = list()
	for x in list1:
		if x not in s:
			list3.append(x)
			mask.append(0)
		else:
			mask.append(1)
	return (mask, list3)

def write_region_info(filename, region_info_list):
	fp = open(filename, 'w')

	region = region_info_list[0]

	fp.write('region id: {0}\n'.format(region.id))
	fp.write('bbox: {0} {1} {2} {3}\n'.format(region.x, region.y, region.w, region.h))
	fp.write('timestamp, confirmed, suppressed, det_cnt, det_sim_cnt, sim_cnt, sim_score:\n')
	for region in region_info_list:
		ts = region.ts
		a1 = region.confirmed
		b1 = region.suppressed
		c1 = region.det_cnt
		d1 = region.det_sim_cnt
		e1 = region.sim_cnt
		f1 = region.sim_score
		fp.write('{0}, {1}, {2}, {3}, {4}, {5}, {6}\n'.format(ts, a1, b1, c1, d1, e1, f1))
	fp.close()

def dump(obj):
  for attr in dir(obj):
    print "obj.%s = %s" % (attr, getattr(obj, attr))

def arg_parse():
	parser = ArgumentParser(description='fd suppression test utility')
	add_arg = parser.add_argument
	add_arg('--input', type=str, required=True, help='path to video or rtsp stream')
	add_arg('--output', type=str, required=True, help='path to output directory')
	add_arg('--verbose', type=bool, required=False, default=False, help='show additional information')
	add_arg('--detection', type=float, required=False, default=0.6, help='detection level')
	add_arg('--write-video', action='store_true', help='write output video')
	add_arg('--show', action='store_true', help='show video')
	add_arg('-t', type=int, required=True, help='maximum seconds to process video')
	args = parser.parse_args()
	return args

def process_frames(video_reader, out_video, fplog, params):

	detection_period = params['detection_period']
	max_seconds = params['max_seconds']
	output_dir = params['output_dir']
	window_name = params['window_name']
	verbose = params['verbose']
	detection_level = params['detection_level']
	write_video = params['write_video']
	show = params['show']

	output_suppressed_dir = os.path.join(output_dir, 'suppressed')
	output_good_dir = os.path.join(output_dir, 'good')
	output_regions_info = os.path.join(output_dir, 'regions_info');

	if not os.path.exists(output_suppressed_dir):
		os.makedirs(output_suppressed_dir)

	if not os.path.exists(output_good_dir):
		os.makedirs(output_good_dir)

	if not os.path.exists(output_regions_info):
		os.makedirs(output_regions_info)

	time_start = time.time()
	last_detection_time = -1.0

	parameters = facesdk.DetectionFilterParameters();
	parameters.sim_threshold = 300;
	# for first rule
	parameters.min_lifetime = 40000;
	parameters.min_det_sim_ratio = 0.3;
	# for second rule
	# parameters.min_sim_det = 25; #minumu simular detections
	# parameters.min_det_freq = 1 / 3.0; # detection per second
	# parameters.min_sim_det_share = 0.45; #simular detection among all detections
	# parameters.bias = 0.0;

	# parameters.intersect_thr = 0.4;
	parameters.confirmed_ttl = 100000;
	# parameters.unconfirmed_ttl = 16000;

	parameters.maxunconfirmed = 10;
	parameters.maxconfirmed = 3;
	parameters.maxsuppressed = 16;

	out_str = 'parameters:\n'
	out_str += 'detection_level: {0}\n'.format(detection_level)
	out_str += 'unconfirmed_ttl: {0}\n'.format(parameters.unconfirmed_ttl)
	out_str += 'confirmed_ttl: {0}\n'.format(parameters.confirmed_ttl)
	out_str += 'suppressed_ttl: {0}\n'.format(parameters.suppressed_ttl)
	out_str += 'img_size: {0}\n'.format(parameters.img_size)
	out_str += 'confirm_after: {0}\n'.format(parameters.confirm_after)
	out_str += 'update_period_confirmed: {0}\n'.format(parameters.update_period_confirmed)
	out_str += 'update_period_suppressed: {0}\n'.format(parameters.update_period_suppressed)
	out_str += 'min_lifetime: {0}\n'.format(parameters.min_lifetime)
	out_str += 'min_sim_det: {0}\n'.format(parameters.min_sim_det)
	out_str += 'min_det_freq: {0}\n'.format(parameters.min_det_freq)
	out_str += 'min_sim_det_share: {0}\n'.format(parameters.min_sim_det_share)
	out_str += 'min_det_sim_ratio: {0}\n'.format(parameters.min_det_sim_ratio)
	out_str += 'intersect_thr: {0}\n'.format(parameters.intersect_thr)
	out_str += 'sim_threshold: {0}\n'.format(parameters.sim_threshold)
	out_str += 'gradient_thr: {0}\n'.format(parameters.gradient_thr)
	out_str += 'bias: {0}\n'.format(parameters.bias)
	out_str += 'maxunconfirmed: {0}\n'.format(parameters.maxunconfirmed)
	out_str += 'maxconfirmed: {0}\n'.format(parameters.maxconfirmed)
	out_str += 'maxsuppressed: {0}'.format(parameters.maxsuppressed)
	print(out_str)
	fplog.write(out_str + '\n')

	detector = facesdk.IDetector();
	print('detector')
	detection_filter = facesdk.IDetectionFilter(parameters);
	print('detection_filter')

	# in BGR format
	color_red = (18, 48, 227);
	color_green = (69, 214, 69);
	color_blue = (200, 54, 7);
	color_yellow = (64, 255, 231);
	color_light_blue = (227, 151, 7);

	regions_info = dict()

	time_measures_detector = collections.deque(maxlen=10)
	time_measures_filter = collections.deque(maxlen=10)
	# for each step
	time_measures_filter_steps = collections.deque(maxlen=10)

	time_feat_extr = 0.0
	time_feat_resize = 0.0
	time_feat_hog = 0.0
	time_classifier = 0.0

	try:
		while True:
			# read next frame
			frame, ts = video_reader.read()
			if not frame:
				break

			time_now = time.time()
			time_delta = time_now - time_start

			img = frame.blob
			img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

			detection_delta = time_now - last_detection_time

			if last_detection_time < 0 or detection_delta >= detection_period:
				last_detection_time = time_now

				out_str = '-----------------------\nkey frame, ts = {0}'.format(ts)
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				# run detection
				start_det = time.time()
				faces = detector.detect(frame, detection_level)
				end_det = time.time()
				time_measures_detector.append(end_det - start_det)

				bboxes, confs = to_bbox_list(faces)

				out_str = 'number of faces: {0}'.format(len(bboxes))
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				start_filt = time.time()
				faces_good = detection_filter.filter(frame, faces, ts)
				end_filt = time.time()
				time_measures_filter.append(end_filt - start_filt)

				# get region id for each face
				associations = detection_filter.getAssociations()
				# get time measures
				measures = detection_filter.getMeasures()
				time_measures_filter_steps.append(measures)
				time_feat_extr = detection_filter.getMeanTimeFeatExtr()
				time_feat_resize = detection_filter.getMeanTimeFeatResize()
				time_feat_hog = detection_filter.getMeanTimeFeatHog()
				time_classifier = detection_filter.getMeanTimeCls()

				bboxes_good, confs_good = to_bbox_list(faces_good)

				out_str = 'number of good faces: {0}'.format(len(bboxes_good))
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				mask_good, bboxes_ignored = list_diff(bboxes, bboxes_good)

				# store associations separatly for ignored and for good faces
				associations_good = list()
				associations_ignored = list()
				confs_ignored = list()
				for idx, flag in enumerate(mask_good):
					if flag:
						associations_good.append(associations[idx])
					else:
						associations_ignored.append(associations[idx])
						confs_ignored.append(confs[idx])

				out_str = 'number of filtered faces: {0}'.format(len(bboxes_ignored))
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				for idx, bbox in enumerate(bboxes_good):

					face_image = frame.lightCrop(bbox[0], bbox[1], bbox[2], bbox[3])
					filename = '{0:0>4}_face_{1}.jpg'.format(associations_good[idx], ts)
					filename = os.path.join(output_good_dir, filename)

					face_image = face_image.blob
					face_image = cv2.cvtColor(face_image, cv2.COLOR_RGB2BGR)
					cv2.imwrite(filename, face_image)

					if show or write_video:
						pt1 = (bbox[0], bbox[1])
						pt2 = (bbox[0] + bbox[2], bbox[1] + bbox[3])
						cv2.rectangle(img, pt1, pt2, color_green, 3)

				for idx, bbox in enumerate(bboxes_ignored):
					face_image = frame.lightCrop(bbox[0], bbox[1], bbox[2], bbox[3])
					filename = '{0}_{1:0>4}_face_{2}.jpg'.format(confs_ignored[idx], associations_ignored[idx], ts)
					filename = os.path.join(output_suppressed_dir, filename)

					face_image = face_image.blob
					face_image = cv2.cvtColor(face_image, cv2.COLOR_RGB2BGR)
					cv2.imwrite(filename, face_image)

					if show or write_video:
						pt1 = (bbox[0], bbox[1])
						pt2 = (bbox[0] + bbox[2], bbox[1] + bbox[3])
						cv2.rectangle(img, pt1, pt2, color_red, 3)

				# regions info
				regions = detection_filter.getRegions()

				to_del = list()
				for region_id, region_info in regions_info.iteritems():
					found = False
					for region in regions:
						if region.id == region_id:
							found = True
							break
					if not found:
						# region is dead, save information
						filename = 'region_{0:0>4}.txt'.format(region_id)
						filename = os.path.join(output_regions_info, filename)
						write_region_info(filename, regions_info[region_id])
						to_del.append(region_id)

				# delete ended regions
				for region_id in to_del:
					del regions_info[region_id]

				for region in regions:
					region.ts = ts
					if region.id in regions_info:
						# region is already tracking, update
						regions_info[region.id].append(region)
					else:
						# create new entry
						regions_info[region.id] = list()
						regions_info[region.id].append(region)

			# draw regions
			if show or write_video:

				# show measure time
				mean_time_detector = 1000 * np.mean(time_measures_detector)
				mean_time_filter = 1000 * np.mean(time_measures_filter)
				mean_time_filter_steps = np.mean(time_measures_filter_steps, axis=0)

				steps_text = ''
				for mean_time_step in mean_time_filter_steps:
					if steps_text:
						steps_text += ' + {0:.1f}'.format(mean_time_step)
					else:
						steps_text += '{0:.1f}'.format(mean_time_step)

				text = 'detector: {0:.2f} ms, filter: {1:.2f} ms = {2}'.format(mean_time_detector, mean_time_filter, steps_text)

				test_size, baseline = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 0.6, 2)
				org = (5, 15 + test_size[1])
				cv2.putText(img, text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)


				time_text = 'feat: {0:.3f} cls: {1:.3f} rz: {2:.3f} hog {3:.3f}'.format(time_feat_extr,
									time_classifier, time_feat_resize, time_feat_hog)

				test_size2, baseline = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 0.6, 2)
				org = (5, 15 + test_size[1] + 5 + test_size2[1])
				cv2.putText(img, time_text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)

				if verbose:
					print('regions number: {0}'.format(len(regions)))
				for idx, region in enumerate(regions):

					# feat = region.feat();
					# if feat is not None:
					# 	feat_img = feat;
					# 	feat_img = cv2.resize(feat_img, (region.w, region.h))
					# 	row1 = region.y;
					# 	row2 = region.y + feat_img.shape[1];
					# 	col1 = region.x;
					# 	col2 = region.x + feat_img.shape[0];

					# 	img[row1:row2, col1:col2, 0] = feat_img;
					# 	img[row1:row2, col1:col2, 1] = feat_img;
					# 	img[row1:row2, col1:col2, 2] = feat_img;

					pt1 = (region.x, region.y)
					pt2 = (region.x + region.w, region.y + region.h)

					ttl_sec = region.ttl * 1.0 / 1000;

					if region.suppressed:
						color = color_yellow
						thickness = 2
					elif region.confirmed:
						color = color_blue
						thickness = 2
					else:
						continue
						color = color_light_blue
						thickness = 1

					cv2.rectangle(img, pt1, pt2, color, thickness)

					# draw text
					thickness = 1
					text = '{0}/{1}/{2}. {3:.1f}, {4:.1f}'.format(region.det_cnt, region.det_sim_cnt, region.sim_cnt, ttl_sec, region.sim_score)

					test_size, baseline = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 0.3, thickness)
					org = (pt1[0], pt1[1] + test_size[1])
					cv2.putText(img, text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.3, color_yellow, thickness)

			if write_video:
				out_video.write(img)
			if show:
				cv2.imshow(window_name, img)

			if time_delta > max_seconds:
				break
	except KeyboardInterrupt:
		print('process interupted')
	finally:
		# properly write all tracked regions
		for region_id, region_info in regions_info.iteritems():
			print('writing {0}...'.format(region_id))
			# region is dead, save information
			filename = 'region_{0:0>4}.txt'.format(region_id)
			filename = os.path.join(output_regions_info, filename)
			write_region_info(filename, regions_info[region_id])


def main():

	args = arg_parse()

	source = args.input
	output_dir = args.output
	verbose = args.verbose
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	video_reader = videoio.IVideoReader(source, 30)

	print('video {0} parameters:'.format(source))
	print(video_reader.width, video_reader.height, video_reader.fps)

	params = dict()
	params['output_dir'] = output_dir
	params['detection_period'] = 1.0 / 6 # in seconds
	params['max_seconds'] = args.t # in seconds
	params['window_name'] = 'preview'
	params['verbose'] = verbose
	params['detection_level'] = args.detection
	params['show'] = args.show
	params['write_video'] = args.write_video

	fplog = open(os.path.join(output_dir, 'log.txt'), 'w')
	# fourcc = cv2.cv.CV_FOURCC(*'XVID')

	# codecArr = list('XVID')
	# fourcc = cv2.cv.CV_FOURCC(ord(codecArr[0]), ord(codecArr[1]), ord(codecArr[2]), ord(codecArr[3]))

	if args.write_video:
		fourcc = cv2.cv.CV_FOURCC('M','J','P','G')
		out_video_filename = os.path.join(output_dir, 'record.avi')
		out_video = cv2.VideoWriter(out_video_filename, fourcc, video_reader.fps, (video_reader.height, video_reader.width))
	else:
		out_video = None

	if args.show:
		cv2.startWindowThread()
		cv2.namedWindow("preview")

	print('start processing.')
	try:
		process_frames(video_reader, out_video, fplog, params)
	except Exception as e:
		print('process error')
		print(str(e))
	fplog.close()
	if args.write_video:
		out_video.release()
		print('save video to {0}'.format(out_video_filename))
	if args.show:
		cv2.destroyAllWindows()
	print('finished.')

if __name__ == '__main__':
	print('fd suppress tool')
	main()