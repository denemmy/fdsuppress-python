# python fdsuppress.py --input 'rtsp://10.10.10.226/h264' --output 'other/output' -t 300

current_epoch=$(date +%s)
target_epoch=$(date -d '03/12/2016 12:00' +%s)
sleep_seconds=$(( $target_epoch - $current_epoch ))
echo 'sleep for ' $sleep_seconds 'seconds'
sleep $sleep_seconds

python fdsuppress.py --input 'rtsp://10.10.10.224/h264' --output 'other/output/test' -t 6000 --detection 0.7 &

current_epoch=$(date +%s)
target_epoch=$(date -d '03/12/2016 13:30' +%s)
sleep_seconds=$(( $target_epoch - $current_epoch ))
echo 'sleep for ' $sleep_seconds 'seconds'
sleep $sleep_seconds

python fdsuppress.py --input 'rtsp://10.10.10.226/h264' --output 'other/output/camera226_14_1' -t 6000 --detection 0.7 &


current_epoch=$(date +%s)
target_epoch=$(date -d '03/12/2016 15:00' +%s)
sleep_seconds=$(( $target_epoch - $current_epoch ))
echo 'sleep for ' $sleep_seconds 'seconds'
sleep $sleep_seconds

python fdsuppress.py --input 'rtsp://10.10.10.221:554/snl/live/1/1' --output 'other/output/camera221_14_1' -t 6000 --detection 0.7 &

current_epoch=$(date +%s)
target_epoch=$(date -d '03/12/2016 16:00' +%s)
sleep_seconds=$(( $target_epoch - $current_epoch ))
echo 'sleep for ' $sleep_seconds 'seconds'
sleep $sleep_seconds

python fdsuppress.py --input 'rtsp://10.10.10.224/h264' --output 'other/output/camera224_14_3' -t 86400 --detection 0.7



