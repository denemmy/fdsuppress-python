#!usr/bin/env python2

import os;
import numpy as np;
import cv2;

print('reading file')
txt_name = '5_3400.txt'
filename = os.path.join('other', 'feats', txt_name)
descr = list()
with open(filename) as f:
	for line in f:
		line = line.strip()
		floats = [float(x) for x in line.split()]
		descr = descr + floats;
print(len(descr))
print('done.')
print('converting to image')
img_sz = 64;

desr_np = np.array(descr).reshape(img_sz, img_sz);
img = np.array(desr_np * 1255, dtype = np.uint8)
print('done.')

print('show image')
cv2.imshow('strong edges', img)
cv2.waitKey(0)
print('done.')
