#!usr/bin/env python2

import pyfacesdk as facesdk
import pyvideoio as videoio
import time
import os
import cv2
import numpy as np
from argparse import ArgumentParser
import scipy
import collections

def to_bbox_list(faces):
	bbox_list = list()
	confs = list()
	for face in faces:
		bbox_list.append((face.x, face.y, face.w, face.h))
		confs.append(face.score)
	return bbox_list, confs

def list_diff(list1, list2):
	s = set(list2)
	list3 = list()
	mask = list()
	for x in list1:
		if x not in s:
			list3.append(x)
			mask.append(0)
		else:
			mask.append(1)
	return (mask, list3)

def dump(obj):
  for attr in dir(obj):
    print "obj.%s = %s" % (attr, getattr(obj, attr))

def arg_parse():
	parser = ArgumentParser(description='fd suppression test utility')
	add_arg = parser.add_argument
	add_arg('--input', type=str, required=True, help='path to video or rtsp stream')
	add_arg('--output', type=str, required=True, help='path to output directory')
	add_arg('--verbose', type=bool, required=False, default=False, help='show additional information')
	add_arg('--detection', type=float, required=False, default=0.6, help='detection level')
	add_arg('--write-video', action='store_true', help='write output video')
	add_arg('--show', action='store_true', help='show video')
	add_arg('-t', type=int, required=True, help='maximum seconds to process video')
	args = parser.parse_args()
	return args

def process_frames(video_reader, out_video, fplog, params):

	detection_period = params['detection_period']
	max_seconds = params['max_seconds']
	output_dir = params['output_dir']
	window_name = params['window_name']
	verbose = params['verbose']
	detection_level = params['detection_level']
	write_video = params['write_video']
	show = params['show']

	output_suppressed_dir = os.path.join(output_dir, 'suppressed')
	output_good_dir = os.path.join(output_dir, 'good')
	output_regions_info = os.path.join(output_dir, 'regions_info');

	if not os.path.exists(output_suppressed_dir):
		os.makedirs(output_suppressed_dir)

	if not os.path.exists(output_good_dir):
		os.makedirs(output_good_dir)

	if not os.path.exists(output_regions_info):
		os.makedirs(output_regions_info)

	last_detection_ts = -1.0

	detector = facesdk.IDetector();
	print('detector')
	detection_filter = facesdk.IDetectionFilter();
	print('detection_filter')

	# in BGR format
	color_red = (18, 48, 227);
	color_green = (69, 214, 69);
	color_blue = (200, 54, 7);
	color_yellow = (64, 255, 231);
	color_light_blue = (227, 151, 7);

	regions_info = dict()

	time_measures_detector = collections.deque(maxlen=10)
	time_measures_filter = collections.deque(maxlen=10)
	# for each step
	time_measures_filter_steps = collections.deque(maxlen=10)

	time_feat_extr = 0.0
	time_feat_resize = 0.0
	time_feat_hog = 0.0
	time_classifier = 0.0

	try:
		while True:
			# read next frame
			frame, ts = video_reader.read()
			if not frame:
				print 'video finished'
				break

			ts_delta = (ts - last_detection_ts) / 1000.0

			img = frame.blob
			img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

			if last_detection_ts < 0 or ts_delta >= detection_period:
				last_detection_ts = ts

				out_str = '-----------------------\nkey frame, ts = {0}'.format(ts)
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				# run detection
				start_det = time.time()
				faces = detector.detect(frame, detection_level)
				end_det = time.time()
				time_measures_detector.append(end_det - start_det)

				bboxes, confs = to_bbox_list(faces)

				out_str = 'number of faces: {0}'.format(len(bboxes))
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				start_filt = time.time()
				faces_good = detection_filter.filter(frame, faces, ts)
				end_filt = time.time()
				time_measures_filter.append(end_filt - start_filt)

				bboxes_good, confs_good = to_bbox_list(faces_good)

				out_str = 'number of good faces: {0}'.format(len(bboxes_good))
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				mask_good, bboxes_ignored = list_diff(bboxes, bboxes_good)

				confs_ignored = list()
				for idx, flag in enumerate(mask_good):
					if  not flag:
						confs_ignored.append(confs[idx])

				out_str = 'number of filtered faces: {0}'.format(len(bboxes_ignored))
				if verbose:
					print(out_str)
				# fplog.write(out_str + '\n')

				for idx, bbox in enumerate(bboxes_good):

					face_image = frame.lightCrop(bbox[0], bbox[1], bbox[2], bbox[3])
					filename = '{0}_face.jpg'.format(ts)
					filename = os.path.join(output_good_dir, filename)

					face_image = face_image.blob
					face_image = cv2.cvtColor(face_image, cv2.COLOR_RGB2BGR)
					cv2.imwrite(filename, face_image)

					if show or write_video:
						pt1 = (bbox[0], bbox[1])
						pt2 = (bbox[0] + bbox[2], bbox[1] + bbox[3])
						cv2.rectangle(img, pt1, pt2, color_green, 3)

				for idx, bbox in enumerate(bboxes_ignored):
					face_image = frame.lightCrop(bbox[0], bbox[1], bbox[2], bbox[3])
					filename = '{0}_{1}_face.jpg'.format(confs_ignored[idx], ts)
					filename = os.path.join(output_suppressed_dir, filename)

					face_image = face_image.blob
					face_image = cv2.cvtColor(face_image, cv2.COLOR_RGB2BGR)
					cv2.imwrite(filename, face_image)

					if show or write_video:
						pt1 = (bbox[0], bbox[1])
						pt2 = (bbox[0] + bbox[2], bbox[1] + bbox[3])
						cv2.rectangle(img, pt1, pt2, color_red, 3)

			# draw regions
			if show or write_video:

				# show measure time
				mean_time_detector = 1000 * np.mean(time_measures_detector)
				mean_time_filter = 1000 * np.mean(time_measures_filter)

				text = 'detector: {0:.2f} ms, filter: {1:.2f} ms'.format(mean_time_detector, mean_time_filter)

				test_size, baseline = cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, 0.6, 2)
				org = (5, 15 + test_size[1])
				cv2.putText(img, text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)

				if verbose:
					print('regions number: {0}'.format(len(regions)))

			if write_video:
				out_video.write(img)
			if show:
				cv2.imshow(window_name, img)

			if ts > 1000.0 * max_seconds:
				break
	except KeyboardInterrupt:
		print('process interupted')


def main():

	args = arg_parse()

	source = args.input
	output_dir = args.output
	verbose = args.verbose
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	video_reader = videoio.IVideoReader(source, 30)

	print('video {0} parameters:'.format(source))
	print(video_reader.width, video_reader.height, video_reader.fps)

	params = dict()
	params['output_dir'] = output_dir
	params['detection_period'] = 1.0 / 6 # in seconds
	params['max_seconds'] = args.t # in seconds
	params['window_name'] = 'preview'
	params['verbose'] = verbose
	params['detection_level'] = args.detection
	params['show'] = args.show
	params['write_video'] = args.write_video

	fplog = open(os.path.join(output_dir, 'log.txt'), 'w')
	# fourcc = cv2.cv.CV_FOURCC(*'XVID')

	# codecArr = list('XVID')
	# fourcc = cv2.cv.CV_FOURCC(ord(codecArr[0]), ord(codecArr[1]), ord(codecArr[2]), ord(codecArr[3]))

	if args.write_video:
		fourcc = cv2.cv.CV_FOURCC('M','J','P','G')
		out_video_filename = os.path.join(output_dir, 'record.avi')
		out_video = cv2.VideoWriter(out_video_filename, fourcc, video_reader.fps, (video_reader.height, video_reader.width))
	else:
		out_video = None

	if args.show:
		cv2.startWindowThread()
		cv2.namedWindow("preview")

	print('start processing.')
	try:
		process_frames(video_reader, out_video, fplog, params)
	except Exception as e:
		print('process error')
		print(str(e))
	fplog.close()
	if args.write_video:
		out_video.release()
		print('save video to {0}'.format(out_video_filename))
	if args.show:
		cv2.destroyAllWindows()
	print('finished.')

if __name__ == '__main__':
	print('fd suppress tool')
	main()